<ol><li>Run the install.sh file first. It will install all the dependencies which are required. </li><br>
<li>Once the code is running go to the website and go to /geturl/</li> <br>
<li>In the post request,send the message in the following format</li> <br>
<h3>POST REQUEST FORMAT</h3>
<pre>
{ <br>
    "source_bucket": "b-ao-product-mock", <br>
    "source_location": "00_00_00.000_00_00_06.000.json" <br>
 }<br>
 </pre>
You'll get a signed url generated <br><br><br><br>
<b>**NOTE**</b> <br>
Download your service account credentials as a json file and make changes in the location of json file in product.py file on line number 12
