# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
# Django rest framework related dependencies
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import TemplateRequestSerializer
from .product import urlcalculation
class TemplateView(APIView):
    def post(self, request, format=None):

        serializer = TemplateRequestSerializer(data=request.data)
        print(serializer.is_valid())
        if serializer.is_valid():
            status_code = status.HTTP_200_OK
            response = urlcalculation().urlcalculation1(serializer.data)
            #print response
            res= {
                "STATUS_CODE": status_code,
                "MESSAGE": "Success msg" ,
                "RESULT": {
                    "PRESIGNED_URL" :  response
                }
            }

            return Response(res)        
        else:
            status_code = status.HTTP_400_BAD_REQUEST
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)