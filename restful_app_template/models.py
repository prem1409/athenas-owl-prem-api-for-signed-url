# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class TemplateRequest(models.Model):
    source_bucket       = models.CharField(max_length=63)
    source_location     = models.CharField(max_length=1000)

class TemplateResponse(models.Model):
    status_code         = models.CharField(max_length=3)
    message             = models.CharField(max_length=1000)

